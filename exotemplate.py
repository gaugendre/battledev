import sys


def main():
    lines = []
    for line in sys.stdin:
        lines.append(line.rstrip("\n"))


if __name__ == "__main__":
    main()
